FROM docker:19.03.8

RUN apk add --update build-base gcc abuild binutils binutils-doc gcc-doc unzip -q
RUN apk add --update python3 python3-dev tar gzip git -q
RUN pip3 install -U pip setuptools --quiet
RUN pip3 install aws-sam-cli -U --quiet

RUN apk add --update python python-dev py-pip -q
RUN pip install -U awscli --quiet

ADD https://github.com/tizz98/gotpl/releases/download/v0.0.1/gotpl_0.0.1_Linux_i386.tar.gz /gotpl.tar.gz
RUN tar -xzf /gotpl.tar.gz
RUN rm /gotpl.tar.gz

RUN apk add --update ruby ruby-bundler ruby-bigdecimal ruby-json -q
RUN gem install ufo -q --silent

RUN sam --version
RUN aws --version
RUN ufo --version
